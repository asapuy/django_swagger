from django.conf import settings
from django.contrib.staticfiles.templatetags.staticfiles import static

SWAGGER_CONFIG = {
    'SPEC_URL': '/swagger/spec.json',
}

CUSTOM_SWAGGER_CONFIG = getattr(settings, 'SWAGGER_CONFIG', {})

SWAGGER_CONFIG.update(CUSTOM_SWAGGER_CONFIG)
SWAGGER_CONFIG['SPEC_URL'] = static(SWAGGER_CONFIG['SPEC_URL'])
