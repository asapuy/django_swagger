# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django_swagger import settings

def index(request):
    return render(request, 'index.html', context=settings.SWAGGER_CONFIG)
