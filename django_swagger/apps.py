# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import AppConfig


class DjangoSwaggerConfig(AppConfig):
    name = 'django_swagger'
