
from setuptools import setup, find_packages
from codecs import open

from os import path

here = path.abspath(path.dirname(__file__))


with open(path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name='django_swagger',
    version='1.0.0',
    description='Django app that loads swagger api description',
    long_description=long_description,
    author='asap developers',
    author_email='info@asap.uy',
    keywords='django swagger',
    packages=find_packages(),
    install_requires=['django']
)
