Django Swagger App
==================

## Instalation

1. Install: `pip install ...`

2. Add django_swagger to INSTALLED_APPS:

  ```
  INSTALLED_APPS = (
    ...
    'django_swagger',
    ...
  )
  ```

3. Add swagger settings:

  ```
  SWAGGER_CONFIG = {
      'SPEC_URL': '/swagger/spec.json',
  }
  ```

  Where:
  - SPEC_URL: is an URL to load from the specification of the API. It can be within another django app, in that case the URL is just the path.

Changelog
---------

* 2017-04-25
  * Initial non funtional version  
* 2017-10-17
  * Add installation in README. Functional version.
